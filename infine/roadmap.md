﻿## User story
Log x
Priorité : haute
Estimation : 0.5 jour
### Definition :
Un stack c'est un list qui contient des entier. il est caracterisé par son id (stack_id)
### Description
En tant que consomateur de l'api, je souhaite ajouter une nouvelle fonctionnalité afin de calculer le log du dernier valeur du stack.
### Scénario
On suppose qu'on a un stack : [ 10, 5, 6 , 10 ]
l'utilisateur a excuté un POST CalculateLog en envoyant l'ID du stack (stack_id)
resultat: stack = [ 10, 5, 6 , 1 ]