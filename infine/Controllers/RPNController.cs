﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using infine.Service;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace infine.Controllers
{
    [Route("rpn")]
    public class RPNController : Controller
    {
        private readonly RPNService rPNService;

        public RPNController(RPNService rPNService)
        {
            this.rPNService = rPNService;
        }
        /// <summary>
        /// List all the operand
        /// </summary>
        [HttpGet("op")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public string[] GetAllOperand()
        {
            return new string[] { "+", "-", "*", "/" };
        }
        /// <summary>
        /// Apply an operand to a stack
        /// </summary>
        [HttpPost("op/{op}/stack/{stack_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult ApplyOperation(string op, Guid stack_id)
        {

            var stack = this.rPNService.GetStackById(stack_id);
            if (stack == null)
            {
                return NotFound("Stack not found");
            }
            if (stack.Data.Count < 2)
            {
                return BadRequest("There is only one element in the stack");
            }

            if (op != "+" && op != "-" && op != "*")
            {
                if (HttpUtility.UrlDecode(op) != "/")
                {
                    return NotFound("Operation not found");
                }
                else
                {
                    op = "/";
                }
            }
            this.rPNService.ApplyOperationOnStack(op, stack);
            return Ok();
        }
        /// <summary>
        /// Create a new stack
        /// </summary>
        [HttpPost("stack")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public ActionResult<Stack> CreateStack()
        {
            return StatusCode(201, this.rPNService.CreateStack());

        }
        /// <summary>
        /// List the available stacks
        /// </summary>
        [HttpGet("stack")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Stack> GetAllStacks()
        {
            return this.rPNService.GetAllStacks();
        }
        /// <summary>
        /// Delete a stack
        /// </summary>
        [HttpDelete("stack/{stack_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteStack(Guid stack_id)
        {
            var stack = this.rPNService.GetStackById(stack_id);
            if (stack == null)
            {
                return NotFound("Stack not found");
            }
            this.rPNService.DeleteStack(stack);
            return Ok();
        }
        /// <summary>
        /// Push a new value to a stack
        /// </summary>
        [HttpPost("stack/{stack_id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult AddValueToStack(Guid stack_id, [FromBody] int value)
        {
            var stack = this.rPNService.GetStackById(stack_id);
            if (stack == null)
            {
                return NotFound("Stack not found");
            }
            this.rPNService.AddNumberToStack(stack, value);
            return Ok();
        }
        /// <summary>
        /// Get a stack
        /// </summary>
        [HttpGet("stack/{stack_id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<Stack> GetStackById(Guid stack_id)
        {
            var stack = this.rPNService.GetStackById(stack_id);
            return stack != null ? stack : NotFound();
        }
    }
}

