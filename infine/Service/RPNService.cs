﻿using System;
using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace infine.Service
{
    public class Stack
    {
        public Guid ID { get; set; }
        public List<int> Data { get; set; }
    }
    public class RPNService
    {
        private readonly List<Stack> _stacks;

        public RPNService()
        {
            this._stacks = new List<Stack>();
        }
        public List<Stack> GetAllStacks()
        {
            return this._stacks;
        }
        public void AddNumberToStack(Stack stack, int number)
        {
            stack.Data.Add(number);
        }

        public void ApplyOperationOnStack(string operation, Stack stack)
        {

            var lastTwoElement = stack.Data.TakeLast(2);
            var result = 0;
            var n1 = lastTwoElement.First();
            var n2 = lastTwoElement.Last();
            if (operation == "+")
            {
                result = lastTwoElement.Sum();
            }
            else if (operation == "-")
            {
                result = n1 - n2;
            }
            else if (operation == "*")
            {
                result = n1 * n2;
            }
            else if (operation == "/")
            {
                if (n2 == 0)
                {
                    throw new Exception("Can't divide by zero");
                }
                result = n1 / n2;
            }
            stack.Data.RemoveRange(stack.Data.Count - 2, 2);
            stack.Data.Add(result);

        }

        public Stack CreateStack()
        {
            var stack = new Stack { ID = Guid.NewGuid(), Data = new List<int> { } };
            this._stacks.Add(stack);
            return stack;

        }

        public void DeleteStack(Stack stack)
        {
            this._stacks.Remove(stack);
        }

        public Stack? GetStackById(Guid stack_id)
        {
            return this._stacks.Find(t => t.ID == stack_id);
        }


    }
}

